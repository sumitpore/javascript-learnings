# JavaScript Learnings with Examples (Mostly related to ES6)

## Table Of Contents
- [JavaScript Learnings with Examples (Mostly related to ES6)](#javascript-learnings-with-examples--mostly-related-to-es6-)
  * [Arrays and Objects - Passed by Reference](#arrays-and-objects---passed-by-reference)
  * [var vs const vs let](#var-vs-const-vs-let)
      - [SCOPING EXAMPLES](#scoping-examples)
      - [VALUE ASSIGNING EXAMPLES](#value-assigning-examples)
  * [Spread Operator](#spread-operator)
  * [Rest Operator](#rest-operator)
  * [for..of & Iterables](#forof---iterables)
  * [Array-like Objects](#array-like-objects)
      - [ARRAY METHODS ON ARRAY-LIKE OBJECTS](#array-methods-on-array-like-objects)
      - [CONVERTING ARRAY-LIKE OBJECTS TO ARRAY](#converting-array-like-objects-to-array)
      - [CREATING CUSTOM ARRAY-LIKE OBJECT](#creating-custom-array-like-object)
  * [Template Literals](#template-literals)
  * [Object Literals](#object-literals)
  * [Destructuring Objects](#destructuring-objects)
  * [Destructuring Arrays](#destructuring-arrays)
  * [Default params](#default-params)
  * [Array includes()](#array-includes--)
  * [Import & Export](#import---export)
      - [NAMED EXPORTS](#named-exports)
      - [DEFAULT EXPORT (one per module)](#default-export--one-per-module-)
      - [MIXED NAMED & DEFAULT EXPORTS](#mixed-named---default-exports)
      - [CYCLICAL DEPENDENCIES](#cyclical-dependencies)
  * [Trailing Commas](#trailing-commas)
  * [Apply vs Bind vs Call](#apply-vs-bind-vs-call)
      - [Apply](#apply)
      - [Call](#call)
      - [Bind](#bind)
  * [Classes](#classes)
    + [Old Methods](#old-methods)
      - [Object Literal Method](#object-literal-method)
      - [Constructor Function Method](#constructor-function-method)
    + [New Method using class keyword](#new-method-using-class-keyword)
  * [References](#references)
  * [Topics Yet to cover](#topics-yet-to-cover)


## Arrays and Objects - Passed by Reference
In JS, Arrays and Objects are passed by references.
```javascript
// Array Example
const array1 = ['FirstElement', 'SecondElement'];
const array2 = array1;
array2.push('ThirdElement');
console.log(array1); //[ 'FirstElement', 'SecondElement', 'ThirdElement' ]
```
```javascript
//Object Example
const object1 = {
    country : 'India',
    city: 'Mumbai'
};
const object2 = object1;
object2.pincode = '400001';
console.log(object1); //{ country: 'India', city: 'Mumbai', pincode: '400001' }
```
___
## var vs const vs let
var: 
  * function/global scoped
  * undefined when accessing a variable before it's declared

let: 
  * block scoped
  * ReferenceError when accessing a variable before it's declared

const:
  * block scoped
  * ReferenceError when accessing a variable before it's declared
  * can't be reassigned
  * Can change the properties inside array or object

> 1. Use `const` by default
> 1. Use `let` if you have to rebind a variable
> 1. use `var` to signal untouched legacy code

#### SCOPING EXAMPLES
```javascript
 // var scope
function greetUser(userName){

    console.log(capitalizedName); //undefined

    var capitalizedName = userName.charAt(0).toUpperCase() + userName.slice(1);

    return capitalizedName;
}

greetUser('Sumit'); 
```
```javascript
// let scope
// EXAMPLE 1
function greetUser(userName){

    console.log(capitalizedName); //referenceError capitalizedName is not defined at greetUser

    let capitalizedName = userName.charAt(0).toUpperCase() + userName.slice(1);

    return capitalizedName;

}

greetUser('Sumit'); 

// EXAMPLE 2
function printNameNtimes(n, name){
    let nameString = '';

    for( let i=0; i<=n - 1; i++) {
        nameString = `${nameString} ${name}`; 
    }

    console.log(i); //uncaught ReferenceError: i is not defined
    return nameString;
}
console.log(printNameNtimes(5, 'Sumit'));
```
```javascript
// const scope
function greetUser(userName){

    console.log(capitalizedName); //ReferenceError capitalizedName is not defined at greetUser

    const capitalizedName = userName.charAt(0).toUpperCase() + userName.slice(1);

    return capitalizedName;
}

greetUser('Sumit'); //eferenceError capitalizedName is not defined at greetUser
```
#### VALUE ASSIGNING EXAMPLES

```javascript
// const premitive datatpye re-assign example
const firstName = 'Sumit';
firstName = 'Akshay'; //TypeError: Assignment to constant variable.
```
```javascript
// const object example
const locationDetails = {
    city : 'Mumbai',
    country: 'India'
};

locationDetails.pincode = 410200;

console.log(locationDetails); // { city: 'Mumbai', country: 'India', pincode: 410200 }
```
```javascript
// const array example
const zipcodes = [410200, 400001, 400002];

zipcodes.push(400003);

console.log(zipcodes); // [ 410200, 400001, 400002, 400003 ]
```
___

## Spread Operator
The spread operator allows an expression to be expanded in places where multiple elements/variables/arguments are expected.


```javascript
var middle = [3, 4];
var arr = [1, 2, ...middle, 5, 6];
console.log(arr);
// [1, 2, 3, 4, 5, 6]
```
```javascript
var arr = ['a', 'b', 'c'];
var arr2 = [...arr];
console.log(arr2);
// ['a', 'b', 'c']
```
```javascript
var arr = ['a', 'b', 'c'];
var arr2 = ['d', 'e', 'f'];
arr = [...arr, ...arr2];
console.log(arr);
// ['a', 'b', 'c', 'd', 'e', 'f']
```
Prefer using arr1 = arr.concat(arr2); to concatenate because its faster.

```javascript
var arr = [2, 4, 8, 6, 0];
var max = Math.max(...arr);
console.log(max); 
// 8
```
```javascript
var str = "hello";
var chars = [...str];
console.log(chars); 
// ['h', 'e',' l',' l', 'o']
```
```javascript
var example1 = {
    firstName: 'Dylan',
    lastName: 'SOmething'
};

var example2 = {
firstName: 'N/A', 
    ...example1
}

console.log(example2)
/**
 * {
 *     firstName: 'Dylan',
 *     lastName: 'SOmething'
 * }
```
```javascript
var example2 = {
 ...example1,
firstName: 'N/A'
}

console.log(example2)
/**
 * {
 *     firstName: 'N/A',
 *     lastName: 'SOmething'
 * }
```
```javascript
var example1 = {
    firstName: 'Dylan',
    lastName: 'SOmething'
};

var example2 = { ...example1 };

console.log(example2);
/**
 * {
 *     firstName: 'Dylan,
 *     lastName: 'SOmething'
 * }
```
To convert Object to Array, use `Array.from(obj)` OR else 
define `iterator` inside the object
___

## Rest Operator
* Similar to spread operator but used to read parameters sent to function.
* Should always be last parameter passed to the function.
* They literally mean "gather the remaining parameters into an array".

```javascript
function showName(firstName, lastName, ...titles) {
  alert( firstName + ' ' + lastName ); // Julius Caesar

  // the rest go into titles array
  // i.e. titles = ["Consul", "Imperator"]
  console.log( titles[0] ); // Consul
  console.log( titles[1] ); // Imperator
  console.log( titles.length ); // 2
}

showName("Julius", "Caesar", "Consul", "Imperator");
```
---
## for..of & Iterables
Using `for..of` loop, we can iterate over iterables

```javascript
const locationDetails = ['India', 'Mumbai', 400001];

for (let detail of locationDetails){
    console.log(detail);
}
// India
// Mumbai
// 400001
```
> Note: If `detail` variable is modified inside the `for..of` block, it won't have any impact on next iteration because it is reset on every iteration.

These are all iterables —
* Arrays and TypedArrays (``Int8Array(),Uint8Array(),Uint8ClampedArray(),Int16Array(),Uint16Array(),Int32Array(),Uint32Array(),Float32Array(),Float64Array()``)
* Strings — iterate over each character or Unicode code-points.
* Maps — iterates over its key-value pairs
* Sets — iterates over their elements
* arguments — An array-like special variable in functions
* DOM elements

Objects are not iterable by default, they are made iterables using `Symbol.iterator`

```javascript
const locationDetails = {
    city : 'Mumbai',
    country: 'India',
    pincode: 400001,
    latitude: 20.593683,
    longitude: 78.962883
};

// 1. call to for..of initially calls this
locationDetails[Symbol.iterator] = function() {

    const listOfValues = Object.values(this);
    console.log(listOfValues);
    const noOfValues = listOfValues.length;
    let currentIndex = 0;
    // ...it returns the iterator object:
   // 2. Onward, for..of works only with this iterator, asking it for next values
    return {
      next() {
          if( currentIndex < noOfValues) {
              return {
                  done: false,
                  value: listOfValues[currentIndex++]
              };
          }

          return {
              done: true
          };
      }  
    }
}

for( let detail of locationDetails){
    console.log(detail);
}

// Mumbai
// India
// 400001
// 20.593683
// 78.962883
```
1. When `for..of` starts, it calls `Symbol.iterator` method once (or errors if not found). The method must return an iterator – an object with the method next.
1. Onward, `for..of` works only with that returned object.
1. When `for..of` wants the next value, it calls `next()` on that object.
1. The result of `next()` must have the form `{done: Boolean, value: any}`, where `done=true` means that the iteration is finished, otherwise value must be the new value.

---
## Array-like Objects
An array-like object
* has: indexed access to elements and the property `length` that tells us how many elements the object has.
* does not have: array methods such as push, forEach and indexOf.

Two examples of array-like objects is the result of the DOM method `document.getElementsByClassName()` (many DOM methods return array-like objects) and the special variable `arguments`. 

* You can apply array methods to these objects using the function `call` method.
* array-like objects can be converted to arrays using `slice` or the ECMAScript 6 `Array.from` method.

> `for..of` loop works with arguments and Dom node objects out of the box but do not work with custom array-like objects. Symbol.iterator is required for custom array-like objects

```javascript
// Basic example of `arguments` variable and how it is accessed in code
function addArgs() {
    var val, sum = 0;
    for ( var i=0, len=arguments.length; i<len; i++ ) {
        val = arguments[i];
        if ( typeof val === 'number' ) { 
            sum += val; 
        }
    }
    return sum;
}

console.log( addArgs(1, 4, 9) );
```
#### ARRAY METHODS ON ARRAY-LIKE OBJECTS

```javascript
// METHOD 1 - DOES NOT WORK
function stringArg() {
    return arguments.join(', ');
}

// TypeError: arguments.join is not a function
console.log( stringArg('Jon', 'Kara', 'Mia') ); 
```
```javascript
//METHOD 2 - invoke array method on arguments object using call method
function stringArg() {
    return Array.prototype.join.call( arguments, ', ');
}
console.log( stringArg('Jon', 'Kara', 'Mia') ); // Jon, Kara, Mia 

```

#### CONVERTING ARRAY-LIKE OBJECTS TO ARRAY
```javascript
// METHOD 1 - invoke array method using call method

// get node list of links
var list = document.querySelectorAll('a');

// convert to array using slice and call
var ar = Array.prototype.slice.call( list );

// then can use array methods
ar.forEach( function(v) {
    v.onclick = function(e) { 
        console.log( e.target.innerHTML ); 
        return false; 
    }
} );
```
```javascript
// METHOD 2 - Using Array.from

// get node list of links
var list = document.querySelectorAll('a');

// convert to array using Array.from
var ar = Array.from( list );

// then can use array methods
ar.forEach( function(v) {
    v.onclick = function(e) { 
        console.log( e.target.innerHTML ); 
        return false; 
    }
} );
```

#### CREATING CUSTOM ARRAY-LIKE OBJECT
```javascript
let arrayLike = { // has indexes and length => array-like
  0: "Hello",
  1: "World",
  length: 2
};

//for..of loop will not work on this array-like object. Therefore
//we'll convert it to array using Array.from.
let convertedArray = Array.from( arrayLike );

for( let value of convertedArray) {
    console.log(value);
}
//Hello
//World

console.log(Array.prototype.join.call( arrayLike, ', ')) // Hello, World
console.log(convertedArray.join(',')); // Hello, World

```
---
## Template Literals

```javascript
const welcomeString = 'Hi';
const userName = 'Sumit';
const welcomeUser = `${welcomeString} ${userName}, Good Morning`;
console.log(welcomeUser); //Hi Sumit, Good Morning
```
---
## Object Literals
```javascript
const city = 'Mumbai';
const country = 'India';

// TRADITIONAL APPROACH
const locationDetails = {
    city: city,
    country: country
}
console.log(locationDetails); //{city: "Mumbai", country: "India"}

// OBJECT LITERALS APPROACH
const locationDetailsNew = {
    city, //If key name and value variable name is same, no need to write them separately
    country
};
console.log(locationDetailsNew); //{city: "Mumbai", country: "India"}
```
---
## Destructuring Objects
```javascript
const locationDetails = {
    'country' : 'India',
    'city': 'Mumbai',
    'pincode': 400001
};
const {country, city} = locationDetails; //country and city are keys present in locationDetails

console.log(country); //India
console.log(city); //Mumbai
```
```javascript
const locationDetails = {
    'country' : 'India',
    'city': 'Mumbai',
    'pincode': 400001
};

const {country: cu, city: cy} = locationDetails; //country and city are keys present in locationDetails

console.log(cu); //India
console.log(cy); //Mumbai
```

## Destructuring Arrays
```javascript
const locationDetails = ['India', 'Mumbai', 400001];

const [country, city, pincode] = locationDetails; //values to variables are assigned index-wise

console.log(country); // India
console.log(city); // Mumbai
console.log(pincode); // 400001
```
## Default params
ES6 supports default parameters for the function
```javascript
const implode = function( array = [], separator = ' ') {
    return array.join(separator);
}

const sampleArray = ['Hello', 'Sumit'];
console.log(implode(sampleArray)); //Hello Sumit
```

## Array includes()
If you want to check if particular value exists or not in array, you can do
`arrayVariable.includes(<value you want to check >)`
It will return boolean value.

But if you want index of that value, you can do
`arrayVariable.indexOf(<value you want to check >)`
If the value does not exist, then it returns -1

```javascript
const sampleArray = ['Hello', 'Sumit'];
console.log(sampleArray.includes('Sumit')); //true
console.log(sampleArray.indexOf('Sumit')) //1
```

## Import & Export
* import and export lets you make your code modular
* You can export modules, variables from one file and then import them in another module

There are 4 types of exports:

* 1— Named exports (several per module) 
* 2— Default exports (one per module)
* 3 — Mixed named & default exports 
* 4— Cyclical Dependencies

#### NAMED EXPORTS
```javascript
//------ lib.js ------
export const sqrt = Math.sqrt;
export function square(x) {
    return x * x;
}
export function diag(x, y) {
    return sqrt(square(x) + square(y));
}
export const sumit = 'SUMIT';

//------ main.js ------
import { square, diag, sumit } from 'lib';
console.log(square(11)); // 121
console.log(diag(4, 3)); // 5
console.log(sumit); // SUMIT

//------ OR ------

//------ main.js (NAMESPACE APPROACH) ------
import * as lib from 'lib';
console.log(lib.square(11)); // 121
console.log(lib.diag(4, 3)); // 5
console.log(sumit); // SUMIT

//------ OR ------

//------ main.js ------
import { square as sq, diag as di, sumit as su } from 'lib';
console.log(sq(11)); // 121
console.log(di(4, 3)); // 5
console.log(su); // SUMIT
```
```javascript
//------ example.js ------
const message1 = {message: "This Is MESSAGE 1"};
const message2 = {message: "This Is MESSAGE 2"};

export {
    message1 as sumit, 
    message2 as ashwini
};
//------ OR ------
export {message1 as sumit};
export {message2 as ashwini};


//------ index.js ----------
import {sumit, ashwini} from './example';
console.log(sumit); //{message: "This Is MESSAGE 1"}
console.log(ashwini); // {message: "This Is MESSAGE 2"}
```

#### DEFAULT EXPORT (one per module)
```javascript
//------ myFunc.js ------
export default function () { ... };

//------ main.js ------
import myFunc from 'myFunc';
myFunc();
```
```javascript
//------ config.js ------
const config = {
    baseurl: 'https://localhost',
    port: 9000
};

export default config;

//------ main.js ------
import config from 'config';
console.log(config);
```
```javascript
//------ shape.js ------
class shape{
    static someFunc(){
        ...
    }
}

export default shape;

//------ main.js ------
import shape from 'shape';
shape.someFunc();
```
#### MIXED NAMED & DEFAULT EXPORTS
```javascript
//------ example.js -------
const ashwini = 'ASHWINI';

export const sqrt = Math.sqrt;

export function square(x) {
    return x * x;
}
export function diag(x, y) {
    return sqrt(square(x) + square(y));
}

export default ashwini;

//------ index.js --------
import ashwini, { square, diag } from './example.js';
console.log(square(11)); // 121
console.log(diag(4, 3)); // 5
console.log(ashwini); // ASHWINI

//------ OR ------
import ashwini, * as lib from './example.js';
console.log(lib.square(11)); // 121
console.log(lib.diag(4, 3)); // 5
console.log(ashwini); // ASHWINI
```
> As a best practise, name of your module should be similar to the export default.

#### CYCLICAL DEPENDENCIES
Cyclical dependencies means that you have 2 files which imports one another. 

```javascript
//------- example.js --------
import Main from './index';
var lib = {message: "This Is A Lib"};
var lib2 = 'SOME MESSAGE'
export default lib;
export {lib2 as Lib2};

//------ index.js ------
import Lib, {Lib2} from './example';
export default class Main {
    //...
}
console.log(Lib); //{message: "This Is A Lib"}
console.log(Lib2); // SOME MESSAGE
```

## Trailing Commas
```javascript
function add(param1,){
    const example = {
        name: 'Dylan',
    };
    
    console.log(example)
};

add(2); //{name: "Dylan"}
```

## Apply vs Bind vs Call

#### Apply
Apply invokes the function and allows you to pass in arguments as an array. Apply executes the current function immediately


```javascript
const person1 = {
    firstName: 'Jon', 
    lastName: 'Kuperman'
};

const person2 = {
    firstName: 'Kelly', 
    lastName: 'King'
};

function say(greeting, question) {
console.log(greeting + ' ' + this.firstName + ' ' + this.lastName + '. ' + question);
}

say.apply(person1, ['Hello', 'How are you doing?']); // Hello Jon Kuperman. How are you doing?
say.apply(person1, ['Hello', 'Whats Up?']); // Hello Kelly King. Whats Up?
```

#### Call
Call invokes the function and allows you to pass in arguments one by one. Call executes the current function immediately

```javascript
const person1 = {
    firstName: 'Jon', 
    lastName: 'Kuperman'
};

const person2 = {
    firstName: 'Kelly', 
    lastName: 'King'
};

function say(greeting, question) {
console.log(greeting + ' ' + this.firstName + ' ' + this.lastName + '. ' + question);
}

say.call(person1, 'Hello', 'How are you doing?'); // Hello Jon Kuperman. How are you doing?
say.call(person2, 'Hello', 'Whats Up?'); // Hello Kelly King. Whats Up?

```

#### Bind
Bind returns a new function, allowing you to pass in a this array and any number of arguments.

The difference between call()/apply() and bind() is that the call()/apply() sets the this keyword and executes the function immediately and it does not create a new copy of the function, while the bind() creates a copy of that function and sets the this keyword.

```javascript
const person1 = {
    firstName: 'Jon', 
    lastName: 'Kuperman'
};

const person2 = {
    firstName: 'Kelly', 
    lastName: 'King'
};

function say(greeting, question) {
console.log(greeting + ' ' + this.firstName + ' ' + this.lastName + '. ' + question);
}

var sayHelloJon = say.bind(person1);
var sayHelloKelly = say.bind(person2);

sayHelloJon('Hello', 'How are you doing?'); // Hello Jon Kuperman. How are you doing?
sayHelloKelly('Hello', 'Whats Up?'); // Hello Kelly King. Whats Up?
```
```javascript
const counter = {
  count: 0,
  incrementCounter: function() {
    console.log(this);
    this.count++;
  }
}
document.querySelector('.btn').addEventListener('click', counter.incrementCounter); //It won't increment counter as `this` inside incrementCounter function can not be evaluated

//---------------------------------------------------

const counter = {
  count: 0,
  incrementCounter: function() {
    console.log(this);
    this.count++;
  }
}
document.querySelector('.btn').addEventListener('click', counter.incrementCounter.bind(counter));
```

## Classes
ES6 class constructors MUST call `super` if they are subclasses.
Static methods can be called without creating a object

### Old Methods

#### Object Literal Method
```javascript
//Using Object Literal Method - NOT RECOMMENDED - JUST Showing possibility - It was used before ES6

const Human = {

    init: function(firstName, lastName, birthdate){
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = new Date(birthdate);
        return this;
    },

	fullName: function(){
		return this.firstName + " " + this.lastName		
    }

}

//Inhertiance in Object literal method
const Player = {
    ...Human, 

    age: function() {
        const today = new Date();
        let age = today.getFullYear() - this.birthdate.getFullYear();
        const m = today.getMonth() - this.birthdate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < this.birthdate.getDate())) {
            age--;
        }
        return age;
    }

}

const player1 = Player.init('Sachin', 'Tendulkar', 'April 24, 1973');
console.log(player1.fullName()); //Sachin Tendulkar
console.log(player1.age()); //45

//Method to object literal can be added externally too.
Player.birthYear = function(){
    return this.birthdate.getFullYear();
}

const player2 = Player.init('Virat', 'Kohli', 'November 5, 1988');
console.log(`${player2.fullName()} born in ${player2.birthYear()}`); //Virat Kohli born in 1988
```
#### Constructor Function Method

You should always stick to the convention that constructor functions start with a capital letter and regular functions with a lowercase letter.
```javascript

 // Creating class ClassA
  ClassA = function() {

  /**
  * This property can only be accessed by private and privileged methods.
  */
  const _privateProperty = 'ITS PRIVATE!';

  /**
  * This property can be accessed publically. Any type of method can access this property. Public Properties start with `this`
  */
  this.publicProperty = 'ITS PUBLIC';

  /**
  * This method can only be called by private and privileged methods.
  */
  const _privateMethod = function() {
    _anotherPrivateMethod();
    console.log('INSIDE _privateMethod');
  };

  const _anotherPrivateMethod = function() {
    console.log('INSIDE _anotherPrivateMethod');
  };

  this.name = 'ClassA';

  /**
  * Privileged methods can access all private, public and other privileged methods and properties. 
  * Public methods can access Privileged methods. They can be called directly by a object of this class. 
  * Privileged methods start with `this`
  * Privileged methods can be overridden by child classes
  */
  this.privilegedMethod = function() {
    console.log('INSIDE this.privilegedMethod');
    _privateMethod();
    this.anotherPublicMethod(); //This method is defined below
    console.log('PRIVATE PROPERTY VALUE: ' + _privateProperty);
    console.log('PUBLIC PROPERTY VALUE: ' + this.publicProperty);
  };

  this.anotherPrivilegedMethod = function() {
    console.log('INSIDE this.anotherPrivilegedMethod');
    this.privilegedMethod();
  };

  this.printClassName = function() {
    console.log(this.name);
  };
};

// Creating publicMethod for classA. Objects of class can directly call this. Can be overrideen in child class. Also known as Prototype Methods
ClassA.prototype.publicMethod = function() {
  console.log('INSIDE ClassA.prototype.publicMethod');
  this.anotherPrivilegedMethod();
};

// Creating anotherPublicMethod for ClassA
ClassA.prototype.anotherPublicMethod = function() {
  console.log('INSIDE ClassA.prototype.anotherPublicMethod');
};


//PROTOTYOPE PROERTIES -- ANYONE MAY READ/WRITE (can be overridden)
ClassA.prototype.prototypeProperty = 'PROTOTYPE PROPERTY';

// STATIC PROPERTIES -- ANYONE MAY READ/WRITE  (Can NOT be overridden). Can be accessed without creating object of a class.
ClassA.staticProperty = 'STATIC PROPERTY';

// Static Method -- ANYONE CAN ACCESS (Can not be overridden). Can be accessed without creating object of a class.
ClassA.staticMethod = function() {
  console.log('INSIDE STATIC METHOD');
};

const classAObject = new ClassA();

classAObject.publicMethod(); 
/**
 * Output of above call:
 * INSIDE ClassA.prototype.publicMethod
 * INSIDE this.anotherPrivilegedMethod
 * INSIDE this.privilegedMethod
 * INSIDE _anotherPrivateMethod
 * INSIDE _privateMethod
 * INSIDE ClassA.prototype.anotherPublicMethod
 * PRIVATE PROPERTY VALUE: ITS PRIVATE!
 * PUBLIC PROPERTY VALUE: ITS PUBLIC
 */


console.log(classAObject.prototypeProperty); //PROTOTYPE PROPERTY

console.log(ClassA.staticProperty); //STATIC PROPERTY

ClassA.staticMethod(); //INSIDE STATIC METHOD

// --------------------------------------------------

//Let's see how to achieve ineritance 

//Creating ClassB
ClassB = function() {

  //Call constructor of parent class - very similar to super()
  //It will inherit instance properties
  ClassA.call(this);

  this.name = 'ClassB';

  //Overriding privileged method inside a class
  this.anotherPrivilegedMethod = function() {
    console.log('INSIDE Class B\'s anotherPrivilegedMethod');
    this.privilegedMethod();
  };
};

//Inheriting from ClassA. After inheriting, prototype (public) as well as privileged methods and properties of ClassA can be accessed by ClassB. It can not access private as well static methods and properties of ClassA

ClassB.prototype =  new ClassA();

//Sometimes people use ClassB.prototype = Object.create(ClassA.prototype); This will not inherit non-prototype methods and prototypes

//Overriding anotherPublicMethod of ClassA
ClassB.prototype.anotherPublicMethod = function(){
    //Calling Super Method
    ClassA.prototype.anotherPublicMethod.call(this);

    console.log('INSIDE ClassB.prototype.anotherPublicMethod');
};

//Overriding privilegedMethod of ClassA using Prototype
ClassB.prototype.privilegedMethod = function() {
    console.log('INSIDE Class B\'s privilegedMethod');
};

const classBObject = new ClassB();

classBObject.publicMethod(); 
/**
 * Output of above call:
 * INSIDE ClassA.prototype.publicMethod
 * INSIDE Class B's anotherPrivilegedMethod
 * INSIDE Class B's privilegedMethod
 */

console.log(classBObject.prototypeProperty); //PROTOTYPE PROPERTY

console.log(classBObject.staticProperty); //undefined

classBObject.staticMethod(); //TypeError: classBObject.staticMethod is not a function

```
When we write `new ClassA()`, what Javascript engine does under the hood is that it makes a copy of our `ClassA` constructor function for each of our objects, each and every property and privileged method is copied to the new instance of the `ClassA`.

If your privileged methods are not accessing private properties or methods, then they are essentially public methods. Using those kind of privileged methods increases memory consumptions as those are copied with every new instantiation of class. Therefore, in that case use `Prototype` methods instead of privilidged methods.

Whenever a new function is created in javascript, Javascript engine by default adds a `prototype` property to it, this property is an object and we call it “prototype object”. By default this prototype object has 
1. a constructor property which points back to our function and
2. another property `__proto__`.

The `__proto__` property is called dunder proto, and it ___POINTS___ to the `prototype` property of our constructor function.

Whenever a new instance of the constructor function is created this `__proto__` property is also copied to the instance along with other properties and methods.

![__proto__ copied on new object creation](https://cdn-images-1.medium.com/max/1200/1*TF3BiUEOt9DPOPJkqas53g.png)

Prototype properties and methods are shared between all the instances of the constructor function, but when any one of the instances of a constructor function makes any change in any primitive property, it will only be reflected in that instance and not among all the instances.

Another thing is that reference type properties are always shared among all the instances, for example, a property of type array, if modified by one instance of the constructor function will be modified for all the instances

If you want to avoid this, define properties inside constructor function.

What happens if you call constructor function without `new` keyword

```javascript
const Person = function (firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
};

const john = new Person('John', 'Doe');
const jane = Person('Jane', 'Doe');

console.log(john); // Person {firstName: 'John' , lastName: 'Doe'}
console.log(jane); // undefined
console.log(window); // Oh dear! The global object now has firstName and lastName!
console.log(window.firstName); //Jane
console.log(window.lastName); //Doe

//-------------------------------------------------

// Protecting against forgetting to include new

// METHOD 1
const Person = function (firstName, lastName) {

    if (!(this instanceof Person)) {
        return new Person(firstName, lastName);
    }

    this.firstName = firstName;
    this.lastName = lastName;
}

// METHOD 2
const Person = function (firstName, lastName) {
    "use strict";
    this.firstName = firstName;// TypeError: Cannot set property 'firstName' of undefined
    this.lastName = lastName;
};
```
### New Method using class keyword
`super` is compulsory inside constructor if extending the class.

With getters, it's a neat way to manipulate data when it's accessed. With setters, it's a neat way to validate data when it's set.

In below example `get name` is getter and `set name` is setter.

`static type` is a static method.

```javascript
class Cat {
    constructor (name, color, gender = 'female') {
        this._name = name;
        this.color = color;
        this.gender = gender;
    }

    present () {
        return `${this._name} is ${this.color}`;
    }

    // name is like a property, but calculatable
    get name () {
        if (this.gender == 'female') {
            return `Ms. ${this._name}`;
        } else {
            return `Mr. ${this._name}`;
        }
    }

    set name (newName) {
        if (newName) {
            this._name = newName;
        } else {
            console.log("I need a name");
        }
    }

    static type(){
        return 'Generic';
    }
}

class Tabby extends Cat {
  constructor (name) {
    super(name, ['Orange', 'White']);
  }
  
  present () {
    return `${super.present()} and she's cool`;
  }

}

let tabby = new Tabby('Friskies Cat', 'Friskies Cat');
console.log(tabby.present()); //Friskies Cat is Orange,White and she's cool
console.log(tabby.name); //Ms. Friskies Cat
tabby.gender = 'male';
tabby.name = 'Mike';
console.log(tabby.name); //Mr. Mike

console.log(Cat.type()); //Generic
console.log(Tabby.type()); //Generic
```

## References
1. https://scrimba.com/g/gintrotoes6
1. https://stackoverflow.com/questions/15455009/javascript-call-apply-vs-bind/15455049
1. https://github.com/happymishra/JavaScriptTutorials/blob/master/Part1/Object.md
1. https://medium.com/tech-tajawal/javascript-classes-under-the-hood-6b26d2667677
1. https://github.com/happymishra/JavaScriptTutorials/blob/master/Part2/Prototypes.md
1. https://www.vojtechruzicka.com/javascript-constructor-functions-and-new-operator/
1. https://stackoverflow.com/questions/6613261/is-there-any-reason-to-use-object-create-or-new-in-javascript
1. https://gist.github.com/cklanac/1433963/68962d66dce0164a48cc959cefe37e227a7d9f36
1. https://github.com/bobleesj/javascript-cheatsheet
1. https://hackernoon.com/import-export-default-require-commandjs-javascript-nodejs-es6-vs-cheatsheet-different-tutorial-example-5a321738b50f
1. https://github.com/DrkSephy/es6-cheatsheet
1. https://github.com/mbeaudru/modern-js-cheatsheet
1. https://ponyfoo.com/articles/es6-classes-in-depth
1. https://github.com/airbnb/javascript
1. https://es2017.io/
1. https://codeburst.io/javascript-the-spread-operator-a867a71668ca

## Topics Yet to cover
- [ ] ES6 Classes
- [ ] Arrow Functions
- [ ] Promises
- [ ] Async & Await
- [ ] Maps and Sets
- [ ] Generators
